% This work is licensed under a Creative Commons Attribution-ShareAlike 4.0
% International License (http://creativecommons.org/licenses/by-sa/4.0/).
%
% Based on source from StackOverflow
% http://stackoverflow.com/questions/2977497/
%

a = 1:3;             % possible numbers
w = [0.3 0.1 0.2];   % corresponding weights
N = 10;              % number of values to generate per iteration
I = 100000;          % number of iterations

w_normalized = w / sum(w)  %# normalised weights, for indication

R=zeros(N * I,1);

fprintf('randsample:\n');
tic
for i=0:(I-1)
    R((1:N)+(i*N)) = randsample(a, N, true, w);
end
toc
tabulate(R)

fprintf('bsxfun:\n');
tic
for i=0:(I-1)
    R((1:N)+(i*N)) = a( sum( bsxfun(@ge, rand(N,1), cumsum(w./sum(w))), 2) + 1 );
end
toc
tabulate(R)

fprintf('histc:\n');
tic
for i=0:(I-1)
    [~, R((1:N)+(i*N))] = histc(rand(N,1),cumsum([0;w(:)./sum(w)]));
end
toc
tabulate(R)

