# MATLAB Weighted Random Number Generation Performance Tests

The code in this repository tests the performance of various methods of generating weighed random numbers in MATLAB.

Based on code from StackExchange question [Weighted random numbers in MATLAB](http://stackoverflow.com/questions/2977497/weighted-random-numbers-in-matlab) and [my answer](http://stackoverflow.com/a/25186850/2433501). Credits to [yuk](http://stackoverflow.com/users/163080/yuk); [Amro](http://stackoverflow.com/users/97160/amro) and user85109.

---

[![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).